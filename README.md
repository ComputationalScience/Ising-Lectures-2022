# Ising Lectures 2022

This repo provides a few coding examples related to my contribution to the Ising Lectures 2022 (see https://www.icmp.lviv.ua/ising/abstracts/bottcher22.html for further information).
